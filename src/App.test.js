import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import App from './App';
import WidgetForm from './WidgetForm/WidgetForm';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<App />, div);
	ReactDOM.unmountComponentAtNode(div);
});

describe('App Component', () => {
	it('should renders WidgetForm', () => {
		const app = shallow(<App />);

		expect(app.find(WidgetForm).length).toBe(1);
	});
})