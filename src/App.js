import React from 'react';
import WidgetForm from './WidgetForm/WidgetForm';

const App = () => {
	return (
		<WidgetForm />
	);
};

export default App;
