const unitTypes = {
	metric: {
		name: 'metric',
		temperature: 'C',
		speed: 'm/s'
	},
	imperial: {
		name: 'imperial',
		temperature: 'F',
		speed: 'mi/h'
	}
};

export default unitTypes;