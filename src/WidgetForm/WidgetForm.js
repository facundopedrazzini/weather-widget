import React, { Component } from 'react'
import unitTypes from '../unitTypes';
import WeatherWidget from '../WeatherWidget/WeatherWidget';
import './WidgetForm.css';


export class WidgetForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			title: '',
			unit: unitTypes.metric.name,
			showWind: true,
			loading: true
		}

		this.fetchWeather();
	}

	handleChange = event => this.setState({ title: event.target.value });

	handleUnitChange = event => this.setState({ unit: event.target.value, loading: true }, () => {
		this.fetchWeather();
	});

	handleShowWindChange = event => this.setState({ showWind: event.target.value === 'true' ? true : false });

	fetchWeather() {
		const { unit } = this.state;

		return new Promise((resolve) => {
			navigator.geolocation.getCurrentPosition((position) => {
				fetch(`http://api.openweathermap.org/data/2.5/weather?appid=480080675ccf8071e03bbbd719640e62&lat=${position.coords.latitude}&lon=${position.coords.longitude}&units=${unit}`)
					.then(res => res.json())
					.then((data) => {
						this.setState({
							name: data.name,
							icon: data.weather[0].icon,
							wind: data.wind,
							main: data.main,
							loading: false
						});
					});
			});
		});
	}

	render() {
		const { title, unit, showWind } = this.state;

		return (
			<div className="container">
				<div className="flex1">
					<p>Title</p>
					<div className="input-container">
						<input type="text" placeholder="Title of widget" onChange={this.handleChange} value={title} />
					</div>
					<p>Temperature</p>
					<div className="input-container">
						<label><input type="radio"
							value={unitTypes.metric.name}
							checked={unit === unitTypes.metric.name}
							onChange={this.handleUnitChange} /> °C</label>
						<label><input type="radio"
							value={unitTypes.imperial.name}
							checked={unit === unitTypes.imperial.name}
							onChange={this.handleUnitChange} /> °F</label>
					</div>
					<p>Wind</p>
					<div className="input-container">
						<label><input type="radio"
							value="true"
							checked={showWind === true}
							onChange={this.handleShowWindChange} /> On</label>
						<label><input type="radio"
							value="false"
							checked={showWind === false}
							onChange={this.handleShowWindChange} /> Off</label>
					</div>
				</div>
				<div className="separator"></div>
				<div className="flex1">
					<WeatherWidget {...this.state} />
				</div>
			</div>
		)
	}
}

export default WidgetForm;
