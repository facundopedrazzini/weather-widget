import React from 'react';
import './WeatherWidget.css';
import unitTypes from '../unitTypes';

const getWindDirection = (deg) => {
	if (deg > 337.5) return 'N';
	if (deg > 292.5) return 'NW';
	if (deg > 247.5) return 'W';
	if (deg > 202.5) return 'SW';
	if (deg > 157.5) return 'S';
	if (deg > 122.5) return 'SE';
	if (deg > 67.5) return 'E';
	if (deg > 22.5) return 'NE';
	return 'N';
};

const WeatherWidget = ({ title, unit, showWind, name, icon, wind, main, loading }) => {
	if (loading) return <div className="card flex column">Loading...</div>

	return (
		<div className="card flex column">
			<h4 className="title">{title || 'Title'}</h4>
			<div className="flex row">
				<div className="weather-image">
					<img alt="Weather Icon" src={`http://openweathermap.org/img/w/${icon}.png`} />
				</div>
				<div className="flex column justify">
					<span className="location-name">{name}</span>
					<span className="temperature">{Math.round(main.temp)}°</span>
					{showWind && <div className="wind"><span>Wind</span> {getWindDirection(wind.deg)} {Math.round(wind.speed) + unitTypes[unit].speed}</div>}
				</div>
			</div>
		</div>
	);
};

export default WeatherWidget;
