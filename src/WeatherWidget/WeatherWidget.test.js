import React from 'react';
import { shallow } from 'enzyme';
import WeatherWidget from './WeatherWidget';

let state = {};

describe('WeatherWidget Component', () => {
	beforeEach(() => {
		state = {
			title: 'someTitle',
			unit: 'metric',
			showWind: true,
			name: 'sydney',
			icon: 'someIcon',
			wind: {
				speed: 40,
				deg: 20
			},
			main: {
				temp: 2
			},
			loading: true
		};
	});

	it('should start with loading', () => {
		const weatherWidget = shallow(<WeatherWidget {...state} />);

		expect(weatherWidget.text()).toBe('Loading...')
	});

	describe('after loading', () => {
		beforeEach(() => {
			state.loading = false;
		});

		describe('title', () => {
			it('should show title', () => {
				const weatherWidget = shallow(<WeatherWidget {...state} />);

				expect(weatherWidget.find('.title').text()).toBe('someTitle')
			});

			it('should show default title when is undefined', () => {
				state.title = undefined;
				const weatherWidget = shallow(<WeatherWidget {...state} />);

				expect(weatherWidget.find('.title').text()).toBe('Title')
			});
		})


		it('should src icon be correct', () => {
			const weatherWidget = shallow(<WeatherWidget {...state} />);

			expect(weatherWidget.find('.weather-image>img').props().src).toBe('http://openweathermap.org/img/w/someIcon.png');
		});

		it('should show name of the location', () => {
			const weatherWidget = shallow(<WeatherWidget {...state} />);

			expect(weatherWidget.find('.location-name').text()).toBe('sydney');
		});

		describe('temperature', () => {
			it('should show temperature', () => {
				const weatherWidget = shallow(<WeatherWidget {...state} />);

				expect(weatherWidget.find('.temperature').text()).toBe('2°');
			});

			describe('round', () => {
				it('should round down temperature correctly', () => {
					state.main.temp = 2.1;
					const weatherWidget = shallow(<WeatherWidget {...state} />);

					expect(weatherWidget.find('.temperature').text()).toBe('2°');
				});

				it('should round up temperature correctly', () => {
					state.main.temp = 2.6;
					const weatherWidget = shallow(<WeatherWidget {...state} />);

					expect(weatherWidget.find('.temperature').text()).toBe('3°');
				});
			});
		});

		describe('wind', () => {
			it('should hide wind when showWind is false', () => {
				state.showWind = false;

				const weatherWidget = shallow(<WeatherWidget {...state} />);

				expect(weatherWidget.find('.wind').length).toBe(0);
			});

			it('should show wind when showWind is true', () => {
				const weatherWidget = shallow(<WeatherWidget {...state} />);

				expect(weatherWidget.find('.wind').text()).toBeDefined();
			});

			it('should use metric units', () => {
				const weatherWidget = shallow(<WeatherWidget {...state} />);

				expect(weatherWidget.find('.wind').text().indexOf('m/s')).toBeGreaterThan(-1);
			});

			it('should use imperial units', () => {
				state.unit = 'imperial';

				const weatherWidget = shallow(<WeatherWidget {...state} />);

				expect(weatherWidget.find('.wind').text().indexOf('mi/h')).toBeGreaterThan(-1);
			});
		});
	});
});
